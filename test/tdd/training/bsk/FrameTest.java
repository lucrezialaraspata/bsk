package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testGetFirstThrow() throws Exception{
		Frame frame = new Frame( 2, 4 );
		
		assertEquals( 2, frame.getFirstThrow() );
	}
	
	@Test
	public void testGetSecondThrow() throws Exception{
		Frame frame = new Frame( 2, 4 );
		
		assertEquals( 4, frame.getSecondThrow() );
	}
	
	@Test
	public void testGetScore() throws Exception{
		Frame frame = new Frame( 2, 4 );
		
		assertEquals( 6, frame.getScore() );
	}
	
	@Test
	public void testGetBonus() throws Exception{
		Frame frame1 = new Frame( 1, 9 );
		Frame frame2 = new Frame( 2, 4 );
		
		frame1.setBonus( frame2.getFirstThrow() );
		
		assertEquals( 2, frame1.getBonus() );
	}
	
	@Test
	public void testIsSpare() throws Exception{
		Frame frame = new Frame( 1, 9 );
		
		assertTrue( frame.isSpare() );
	}
	
	@Test
	public void testGetScoreIncludingBonus() throws Exception{
		Frame frame1 = new Frame( 1, 9 );
		Frame frame2 = new Frame( 2, 4 );
		
		frame1.setBonus( frame2.getFirstThrow() );
		
		assertEquals( 12, frame1.getScore() );
	}
	
	@Test
	public void testIsStrike() throws Exception{
		Frame frame = new Frame( 10, 0 );
		
		assertTrue( frame.isStrike() );
	}
	
	@Test
	public void testGetScoreWithStrike() throws Exception{
		Frame frame1 = new Frame( 10, 0 );
		Frame frame2 = new Frame( 2, 4 );
		
		frame1.setBonus( frame2.getScore() );
		
		assertEquals( 16, frame1.getScore() );
	}

}
