package tdd.training.bsk;

import java.util.*;

public class Game {
	
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	private final static int DEFAULT_BONUS_THROW = 0;
	

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new ArrayList<Frame>();
		this.firstBonusThrow = DEFAULT_BONUS_THROW;
		this.secondBonusThrow = DEFAULT_BONUS_THROW;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frames.add( frame );
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return this.frames.get( index );	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int gameScore = 0;
		Frame lastFrame = this.frames.get( this.frames.size() - 1 );
		
		if( this.isPerfectGame() ) return 300;
		
		for( int i = 0 ; i < this.frames.size() ; i++ ) {
			Frame frame = this.frames.get( i );
			
			if( frame.isStrike() && ( i != ( this.frames.size() - 1 ) )) {
				if( this.frames.get( i + 1).isStrike() ) {
					frame.setBonus( this.frames.get( i + 1).getScore()  + this.frames.get( i + 2).getFirstThrow()  );
				}
				else	frame.setBonus( this.frames.get( i + 1).getScore() );
			}
			else if( frame.isSpare() && ( i != ( this.frames.size() - 1 ) ) )
				frame.setBonus( this.frames.get( i + 1).getFirstThrow() );
			gameScore += frame.getScore();
		}
		
		if( lastFrame.isStrike() ) {
			gameScore += this.firstBonusThrow;
			gameScore += this.secondBonusThrow ;
		}
		else if( lastFrame.isSpare() ) gameScore += this.firstBonusThrow;
		

		
		return gameScore;	
	}
	
	public boolean isPerfectGame() throws BowlingException {
		int j = 0;
		
		if( ( this.firstBonusThrow == 10 ) && ( this.secondBonusThrow == 10 ) ) 	j += 2;
		
		for( int i = 0 ; i < this.frames.size() ; i++ ) {
			Frame frame = this.frames.get( i );
			
			if( frame.isStrike() ) j++;
		}
		
		if( j == 12 ) return true;
		
		return false;
		
	}

}
